# Metrocheck client

Vue.Js frontend for [metrocheck](metrocheck.app)

It uses Mapbox GL and Turf to display data on the map.
The data is fetched from an [api](https://gitlab.com/metrocheck/public/api) and stored in [indexed db](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API) using [dexie](dexie.org).

## Development

### Getting started

**Requirements**
* Having Docker installed
* Having the [API](https://gitlab.com/metrocheck/public/api) up and running

Install dependencies
```
yarn install
```

Create a .env file at the root of the project : 
```.env
VUE_APP_MAPBOX_TOKEN="<YOURTOKEN>"
VUE_APP_ACKEE_ID="<OPTIONALID>"
```

Compile/serve
```shell script
# Compiles and hot-reloads for development
yarn serve

# Compiles and minify for production
yarn build
```

## License
ISC
