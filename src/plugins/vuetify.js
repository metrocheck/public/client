import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import styles from '../../config/styles'

Vue.use(Vuetify)

export default new Vuetify({
    theme: {
        dark: {
            primary: styles.colors.orange,
            secondary: styles.colors.blue,
            accent: '#cddc39',
            error: '#f44336',
            warning: styles.colors.yellow,
            info: '#00bcd4',
            success: '#8bc34a',
        },
    },
})
