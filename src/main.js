import Vue from 'vue'
import Vuex from 'vuex'
import VuexFlash from 'vuex-flash'
import VueRouter from 'vue-router'
import VueIdb from 'vue-idb'
import DatetimePicker from 'vuetify-datetime-picker'
import App from './components/App.vue'
import vuetify from './plugins/vuetify'
import VueWorker from 'vue-worker'
import routes from './routes'
import store from './store'
import dexie from './store/dexie'

const debug = process.env.NODE_ENV !== 'production'

Vue.config.productionTip = !debug

Vue.use(Vuex)
Vue.use(VueIdb)
Vue.use(VueRouter)
Vue.use(VuexFlash, { template: '<h1>{{ message }}</h1>' })
Vue.use(DatetimePicker)
Vue.use(VueWorker)

const router = new VueRouter({
  routes,
})

new Vue({
  vuetify,
  router,
  idb: dexie,
  store: new Vuex.Store(store),
  render: h => h(App),
}).$mount('#app')
