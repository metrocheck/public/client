import moment from 'moment-timezone'
import { mapGetters, mapState } from 'vuex'
import { getters } from '../store/constants'

export default {

    data: function () {
        return {
            loading: true,
        }
    },

    computed: {
        ...mapState(['displayedDatetime', 'hydrated']),
        ...mapGetters([getters.GET_LINE_BY_ID]),

        date: function () {
            return moment(this.displayedDatetime).format('YYYY-MM-DD')
        },
        wakeUpTime: function () {
            return this.stats && this.stats.wakeUpTime ? moment(this.stats.wakeUpTime, 'HH:mm:i') : null
        },
        sleepTime: function () {
            return this.stats && this.stats.sleepTime ? moment(this.stats.sleepTime, 'HH:mm:i') : null
        },
    },

    watch: {
        displayedDatetime: function (n, o) {
            console.debug('displayedDatetime change seen in Line')
            this.loading = true
            return this.fetchData().finally(() => { this.loading = false })
        },
    },
}
