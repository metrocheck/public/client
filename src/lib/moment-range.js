import Moment from 'moment-timezone'
import { extendMoment } from 'moment-range'

export default extendMoment(Moment)
