export default class ApiCommunicationError extends Error {
    constructor (message) {
        super(message)
        this.name = 'ApiCommunicationError'
    }
}
