/* eslint camelcase:0 */
import moment from '../../lib/moment-range'
import { fetchPositions as apiFetchPositions } from '../../api'
import Dexie from 'dexie'
import { midpoint, nearestPointOnLine, point } from '@turf/turf'
import ApiCommunicationError from '../../lib/ApiCommunicationError'

const fetchIntervalMs = 20000
let db

const fetchPositions = async ({ from, to, lines }) => {
    // console.log('Worker fetchPositions.', to, from, lines)
    const response = await apiFetchPositions(lines, moment(from).unix(), moment(to).unix())
    // console.log(response)

    if (!response || !response.status || response.status !== 200) {
        throw new ApiCommunicationError(`Received status ${response.status}`)
    }

    const processedPositions = await Promise.all(response.data.map(processFetchedPosition))
    // console.log(processedPositions)

    return processedPositions
}

const processFetchedPosition = async p => {
    p.datetime = moment(p.datetime).valueOf()
    if (p.distance_from_point > 0) {
        try {
            // noinspection UnnecessaryLocalVariableJS
            const computedGeolocation = await getTravelingIconGeolocation(p)
            // console.debug('Vehicle in transit. computedGeolocation : %O', computedGeolocation)
            p.point_geolocation = computedGeolocation || p.point_geolocation
        } catch (e) {
            console.log('Failed to get next point. Metro run is not described. position : %O ; Error : %O', p, e.message)
        }
    }

    return p
}

const getTravelingIconGeolocation = async ({ line_fk, point_fk, direction_fk }) => {
    // console.debug('getOnTravelIconGeolocation', line_fk, direction_fk, point_fk)

    // Get point name
    const departurePoint = await db.table('points').where('id').equals(point_fk).first()
    const directionPoint = await db.table('points').where('id').equals(direction_fk).first()

    if (!departurePoint || !directionPoint) {
        console.info(`${departurePoint ? 'departurePoint' : 'directionPoint'} ${departurePoint ? point_fk : direction_fk} not found`)
        return false
    }

    // console.log(directionPoint)

    // Get next point
    const arrivalPoint = await getNextPointOnLine({ line_fk, point_fk, direction_name_fr: directionPoint.name_fr })

    if (!arrivalPoint) {
        console.info(`No station found following ${departurePoint.name_fr} on line ${line_fk}${departurePoint.line_fk !== directionPoint.line_fk ? ` - !${directionPoint.line_fk}!` : ''}`)
        return false
    }

    // Get middle point
    // console.debug('Calculating midpoint position between stations %s(%i) and %s(%i)', departurePoint.name_fr, departurePoint.id, arrivalPoint.name_fr, arrivalPoint.id)
    const pointsMidpoint = midpoint(
        point([departurePoint.lon, departurePoint.lat]),
        point([arrivalPoint.lon, arrivalPoint.lat]))

    // Get corresponding point on line
    try {
        const line = self.geojsonSources[line_fk].features[0]
        const nearestPointOnLineCoordinates = nearestPointOnLine(line, pointsMidpoint, { units: 'degrees' }).geometry.coordinates

        // console.debug('departureCoordinates : %O, %O', departurePoint.lon, departurePoint.lat)
        // console.debug('arrivalCoordinates : %O, %O', arrivalPoint.lon, arrivalPoint.lat)
        // console.debug('midPointCoordinates : %O', pointsMidpoint)
        // console.debug('midPointOnLineCoordinates : %O', nearestPointOnLineCoordinates)
        // console.debug('line: %O', line)

        return { x: nearestPointOnLineCoordinates[1], y: nearestPointOnLineCoordinates[0] }
    } catch (e) {
        console.error(e)
    }
}

const getNextPointOnLine = async ({ line_fk, point_fk, direction_name_fr }) => {
    try {
        // console.debug('[getNextPointOnLine] line_fk: %i, point_fk: %i, direction_name_fr: %s', line_fk, point_fk, direction_name_fr)

        const linePoint = await db.table('linepoints')
            .where('[line_fk+point_fk+direction_name_fr]').equals([line_fk, point_fk, direction_name_fr]) // eslint-disable-line camelcase
            .first()

        if (!linePoint) {
            console.warn('No order found for station %i on line %i direction %s', point_fk, line_fk, direction_name_fr)
            return null
        }

        const arrivalPoint = await db.table('linepoints')
            .where('[line_fk+order+direction_name_fr]').equals([line_fk, linePoint.order + 1, direction_name_fr]) // eslint-disable-line camelcase
            .first()

        if (!arrivalPoint) {
            console.warn('No next point seems to be available...')
            return null
        }

        // console.log('[getNextPointOnLine] arrivalPoint : %O', arrivalPoint)

        const nextPoint = await db.table('points').where('id').equals(arrivalPoint.point_fk).first()

        return nextPoint
    } catch (e) {
        console.error(e)
        console.info('Next point not found on line %i point %i towards %s', line_fk, point_fk, direction_name_fr)
    }
}

const parseArguments = function ({ from, to, lines, fetched }) {
    if (!from) {
        throw new Error('From delimiter must be supplied')
    }

    if (!lines || !Array.isArray(lines)) {
        throw new Error('Lines must be an array')
    }

    if (!to) {
        // console.debug('PositionWorker : Only one time delimiter supplied. Generating delimiters according to minimum threshold')
        to = moment(from).valueOf()
        // to = moment(from).add(fetchIntervalMs / 2, 'milliseconds').valueOf()
        // from = moment(from).subtract(fetchIntervalMs / 2, 'milliseconds').valueOf()
        from = moment(from).subtract(fetchIntervalMs, 'milliseconds').valueOf()
    }

    return { from, to, lines, fetched }
}

const getFromIdb = async function ({ from, to, lines }) {
    // console.debug('getFromIdb', from, to, lines)
    const positions = await db.table('positions').where('datetime').between(from, to).and(({ line_fk }) => lines.includes(line_fk)).toArray()
    // console.debug(positions)

    return positions
}

const sendPositions = function (positions) {
    // console.debug('Sending DISPLAY_POSITIONS message', positions)
    postMessage({
        type: 'DISPLAY_POSITIONS',
        positions: positions || [],
    })
}
const sendMessage = function (message) {
    // console.debug('Sending DISPLAY_MESSAGE message', message)
    postMessage({
        type: 'DISPLAY_MESSAGE',
        message,
    })
}

const persistPositions = function (positions) {
    // console.debug('Sending PERSIST_POSITIONS message', positions)
    postMessage({
        type: 'PERSIST_POSITIONS',
        positions,
    })
}

const persistFetchedTimerange = function (from, to) {
    // console.debug('Sending PERSIST_FETCHED_RANGE message')
    postMessage({
        type: 'PERSIST_FETCHED_RANGE',
        from,
        to,
    })
}

addEventListener('message', async ({ data }) => {
    const { action, args } = data
    // console.debug('Worker received message %O', action)

    switch (action) {
        case 'SET_GEOJSON_SOURCES':
            // console.debug('[WORKER] Set geojson sources')
            // console.log(args.geojsonSources)
            self.geojsonSources = args.geojsonSources
            break
        case 'POSITIONS_AT':
            let positions
            const { from, to, lines, fetched } = parseArguments(args)
            // console.debug('[WORKER] Handling POSITIONS_AT. from : %O, to : %O, lines : %O, fetched: %O', moment(from).format(), moment(to).format(), lines, fetched)

            db = new Dexie('metrocheck')
            await db.open()

            // Check if datetime range is in fetched list
            const itemTimeSpan = moment.range(from, to)
            console.debug('[WORKER] TODO: Check if datetime range is in fetched list')
            const datetimeRangeFetched = fetched ? moment.range(fetched).contains(itemTimeSpan, {
                excludeStart: false,
                excludeEnd: false,
            }) : false

            // Check if there is matching data
            positions = await getFromIdb({ from, to, lines })

            if (positions.length || datetimeRangeFetched) { // There is data, or range is known. Returning positions
                // console.debug(`[WORKER] Sending ${positions.length} positions. Time range marked as ${datetimeRangeFetched ? '' : 'not'} fetched`)
                return sendPositions(positions)
            }

            // There is no data and range is not known.

            // Fetching data
            try {
                positions = await fetchPositions({ from, to, lines })
                // console.debug(`Fetched ${positions.length} positions`)
            } catch (e) {
                console.error('[WORKER] fetchPositions failed. %O', e)
            }

            // Marking span as fetched
            persistFetchedTimerange(from, to)

            if (!positions || !positions.length) {
                return sendMessage('Looks like we are experiencing troubles to get data for this time range... Plz wait for refresh')
            }

            persistPositions(positions)
            return sendPositions(positions)
        default:
            console.error(`action ${action} not understood`)
    }
})
