const worker = new Worker('@/workers/positions/worker.js', { type: 'module' })

const send = ({ action, args }) => worker.postMessage({
    action, args,
})

export default {
    worker,
    send,
}
