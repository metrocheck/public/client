const axios = require('axios')
const config = require('../config/index').default
const moment = require('./lib/moment-range').default

const routeToUrl = route => `${config.api.protocol}://${config.api.host}:${config.api.port}${route}`

const simpleFetch = url => {
    // console.info(`About to fetch ${url}`)
    return axios.get(url)
        .then((response) => {
            // console.debug(`API returned ${response.data.length} records`)
            return Promise.resolve(response)
        })
}

module.exports = {

    fetchLines: () => {
        const url = routeToUrl(config.api.routes.lines)
        return simpleFetch(url)
    },

    fetchPoints: () => {
        const url = routeToUrl(config.api.routes.points.replace(':lines', config.supportedLines.join(',')))
        return simpleFetch(url)
    },

    fetchLinePoints: () => {
        const url = routeToUrl(config.api.routes.linePoints)
        return simpleFetch(url)
    },

    fetchPositions: (lines, from, to) => {
        const url = routeToUrl(config.api.routes.positions
            .replace(':lines', lines.join(','))
            .replace(':from', from)
            .replace(':to', to),
        )

        return simpleFetch(url)
    },

    fetchAvailableDates: () => {
        const url = routeToUrl(config.api.routes.availableDates.replace(':lines', config.supportedLines.join(',')))

        // console.info(`About to fetch ${url}`)

        return axios.get(url)
            .then(({ data }) => {
                // console.debug(`API returned ${data.length} available dates`)
                return Promise.resolve(data.map(date => moment(date).format('YYYY-MM-DD')))
            })
            .catch(console.error)
    },

    fetchLineStatistics: (line, date) => {
        const url = routeToUrl(config.api.routes.statistics.line
            .replace(':line', line)
            .replace(':date', date),
        )

        console.info(`About to fetch ${url}`)

        return axios.get(url)
            .then(({ data }) => {
                // if (!data || !data.length) { throw new Error('No data') }

                console.debug('API returned statistics')
                return Promise.resolve(data)
            })
    },

    fetchPointsStatistics: (ids, date) => {
        const url = routeToUrl(config.api.routes.statistics.point
            .replace(':ids', ids)
            .replace(':date', date),
        )

        console.info(`About to fetch ${url}`)

        return axios.get(url)
            .then(({ data }) => {
                if (!data || !Object.keys(data).length) { console.log(data); throw new Error('No data in fetchPointsStatistics') }

                console.debug('API returned statistics')
                return Promise.resolve(data)
            })
    },

}
