import moment from '../lib/moment-range'
import { mutations as m } from './constants'
import db from './dexie'

export default {
    [m.SET_DISPLAYED_DATETIME] (state, newValue) {
        // console.debug('m.SET_DISPLAYED_DATETIME', moment(newValue).format())
        state.displayedDatetime = newValue
    },

    [m.SET_ALLOWED_DATES] (state, dates) {
        state.allowedDates = dates
    },

    [m.SET_LINE_STATISTIC] (state, { date, line, statistics }) {
        const currentValue = state.lineStatistics[date]
        const addedValue = { [line]: statistics }

        if (!currentValue) {
            // state.lineStatistics[date] = addedValue
            state.lineStatistics = { [date]: addedValue }
        } else {
            state.lineStatistics = { ...state.lineStatistics, [date]: addedValue }
        }

        console.debug('line stats updated : %O', state.lineStatistics)
    },

    [m.SET_POINT_STATISTIC] (state, { point, date, statistic }) {
        if (!state.pointStatistics[date]) { state.pointStatistics[date] = {} }

        state.pointStatistics[date] = Object.assign(state.pointStatistics[date], { [point]: statistic })

        console.debug('point stats updated for point : %O date %O', point, date)
    },

    [m.SET_POSITIONS_LOADING] (state, value) {
        state.arePositionsLoading = value
    },

    [m.SET_LINE_GEOJSON] (state, { index, data }) {
        state.geojsonSources[index.toString()] = data
    },

    [m.SET_IS_REAL_TIME] (state, val) {
        state.isRealTime = val
    },

    [m.ADD_FETCHED_TIME_RANGE] (state, { from, to }) {
        // console.debug('m.ADD_FETCHED_TIME_RANGE, %O, %O', from, to)
        const range = moment.range(from, to)
        if (!state.fetchedRange) {
            state.fetchedRange = range.toString()
        } else {
            const newRange = moment.range(state.fetchedRange)
            newRange.add(range)
            state.fetchedRange = newRange.toString()
        }
    },

    [m.SET_POSITIONS] (state, positions) {
        // console.debug('m.SET_POSITIONS %O', positions)
        db.db.positions.bulkAdd(positions)
            // .then(console.debug)
            .catch('ConstraintError', e => {
                // Failed with ConstraintError
                console.error('ConstraintError  error: ' + e.message)
            })
            .catch('BulkError', e => {
                // Failed with ConstraintError
                console.error('Bulk error: ' + e.message)
            }).catch(Error, e => {
            // Any other error derived from standard Error
            console.error('Error: ' + e.message)
            throw e
        }).catch(e => {
            // Other error such as a string was thrown
            console.error(e)
            throw e
        })
    },
}
