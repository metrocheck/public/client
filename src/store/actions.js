import { fetchPointsStatistics } from '../api'
import { actions as a, mutations as m, mutations } from './constants'

export default {

    /* API-related actions */

    [a.FETCH_LINES_GEOJSON]: async ({ commit }) => {
        async function execFetch (url, index) {
            const response = await fetch(url)
            const responseJson = await response.json()
            commit(m.SET_LINE_GEOJSON, { index, data: responseJson })
            return { index, data: responseJson }
        }

        return Promise.all(
            [1, 2, 5, 6].map(lineNumber => execFetch(`/json/line${lineNumber}.geo.json`, lineNumber))
                .concat([execFetch('/json/technical.geo.json', 'technical')]),
        )
    },

    [a.FETCH_POINT_STATISTICS]: async ({ commit, dispatch, getters }, { points, date }) => {
        const statistics = await fetchPointsStatistics(points, date)
        console.log(statistics)
        const { wakeUpTime, sleepTime, frequency } = statistics

        Object.keys(statistics.frequency).forEach(point => {
            commit(mutations.SET_POINT_STATISTIC, { point, date, statistic: { wakeUpTime, sleepTime, frequency: frequency[point] } })
        })

        return Promise.resolve()
    },
}
