export const mutations = {
    SET_DISPLAYED_DATETIME: 'SET_DISPLAYED_DATETIME',
    SET_ALLOWED_DATES: 'SET_ALLOWED_DATES',

    SET_LINE_STATISTIC: 'SET_LINE_STATISTIC',
    SET_POINT_STATISTIC: 'SET_POINT_STATISTIC',

    SET_POINTS_LOADING: 'SET_POINTS_LOADING',
    SET_POSITIONS_LOADING: 'SET_POSITIONS_LOADING',

    SET_LINE_GEOJSON: 'SET_LINE_GEOJSON',
    SET_IS_REAL_TIME: 'SET_IS_REAL_TIME',
    ADD_FETCHED_TIME_RANGE: 'ADD_FETCHED_TIME_RANGE',

    SET_POSITIONS: 'SET_POSITIONS',
}

export const actions = {
    FETCH_LINES_GEOJSON: 'FETCH_LINES_GEOJSON',
    FETCH_POSITIONS_BY_DATETIME: 'FETCH_POSITIONS_BY_DATETIME',
    FETCH_POINT_STATISTICS: 'FETCH_POINT_STATISTICS',
    GET_POSITIONS_BY_DATETIME_LINE: 'GET_POSITIONS_BY_DATETIME_LINE',
    GET_TRAVELING_ICON_GEOLOCATION: 'GET_TRAVELING_ICON_GEOLOCATION',
}

export const getters = {
    GET_LINE_BY_ID: 'GET_LINE_BY_ID',
    GET_LINES_BY_POINT_ID: 'GET_LINES_BY_POINT_ID',

    GET_POINT_BY_ID: 'GET_POINT_BY_ID',
    GET_POINTS_BY_NAME: 'GET_POINTS_BY_NAME',
    GET_POINTS_BY_LINE: 'GET_POINTS_BY_LINE',

    GET_NEXT_POINT: 'GET_NEXT_POINT',
}
