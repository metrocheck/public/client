import config from '../../config'

export default {
    displayedDatetime: Date.now() - config.api.fetchIntervals.positions,
    isRealTime: true,

    allowedDates: [],

    lineStatistics: {},
    pointStatistics: {},

    geojsonSources: {},

    arePointsLoading: false,
    arePositionsLoading: false,

    hydrated: false,

    fetchedRange: null,
}
