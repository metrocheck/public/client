import VueIdb from 'vue-idb'
import { fetchPoints, fetchLines, fetchLinePoints } from '../api'

const idb = new VueIdb({
    version: 1,
    database: 'metrocheck',
    schemas: [
        { lines: 'id' },
        { points: 'id, name_fr, name_nl' },
        { linepoints: '[line_fk+point_fk+direction_name_fr], [line_fk+order+direction_name_fr], point_fk' },
        { positions: 'id, datetime, line_fk, point_fk' },
    ],
    options: {
        lines: { type: 'list', primary: 'id', label: 'id' },
        points: { type: 'list', primary: 'id', label: 'id' },
        linepoints: { type: 'list' },
        positions: { type: 'biglist', primary: 'id', created: 'created_at', updated: 'updated_at' },
    },
    apis: {
        points: {
            all: () => fetchPoints(),
        },
        lines: {
            all: () => fetchLines(),
        },
        linepoints: {
            all: () => fetchLinePoints(),
        },
    },
})

export default idb
