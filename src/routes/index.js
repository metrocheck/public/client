import Stats from './stats/Stats'
import Map from './Map'
import NotFound from './NotFound'

const routes = [
    {
        path: '/stats',
        component: Stats,
        name: 'stats',
        children: [
            {
                path: '/line/:lineId',
                component: Stats,
                name: 'line',
            },
            { path: '/point/id/:id', component: Stats, name: 'pointId' },
            { path: '/point/:pointName', component: Stats, name: 'point' },
        ],
    },

    { path: '/', component: Map, name: 'map' },

    { path: '*', component: NotFound, name: 'notfound' },
]

export default routes
