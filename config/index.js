import settings from './settings.default'
import api from './api'
import map from './map'
import styles from './styles'

export default {
    ...settings,
    api,
    map,
    styles,
    supportedLines: [1, 2, 5, 6],
    fetchedTimeChunk: 10 * 60 * 1000, // 10min
}
