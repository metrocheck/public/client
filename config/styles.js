const colors = {
    orange: '#F57000',
    blue: '#0078AD',
    yellow: '#E6B012',
    purple: '#C4008F',
    darkBlue: '#00091e',

}

const styles = {
    colors,
}

export default styles
