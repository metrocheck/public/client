export default {
     protocol: 'https',
     host: 'api.metrocheck.app',
     port: '443',
    fetchIntervals: {
      positions: 20 * 1000,
    },
    routes: {
        lines: '/lines',
        points: '/points/:lines',
        linePoints: '/line-points',
        positions: '/positions/:lines/:from/:to',
        availableDates: '/dates/:lines',
        statistics: {
            line: '/statistics/line/:line/:date',
            point: '/statistics/point/:ids/:date',
        },
    },
}
